package ru.t1.bondarenko.tm.component;

import ru.t1.bondarenko.tm.api.component.IBootstrap;
import ru.t1.bondarenko.tm.api.controller.ICommandController;
import ru.t1.bondarenko.tm.api.controller.IProjectController;
import ru.t1.bondarenko.tm.api.controller.IProjectTaskController;
import ru.t1.bondarenko.tm.api.controller.ITaskController;
import ru.t1.bondarenko.tm.api.repository.ICommandRepository;
import ru.t1.bondarenko.tm.api.repository.IProjectRepository;
import ru.t1.bondarenko.tm.api.repository.ITaskRepository;
import ru.t1.bondarenko.tm.api.service.ICommandService;
import ru.t1.bondarenko.tm.api.service.IProjectService;
import ru.t1.bondarenko.tm.api.service.IProjectTaskService;
import ru.t1.bondarenko.tm.api.service.ITaskService;
import ru.t1.bondarenko.tm.constant.ArgumentConstant;
import ru.t1.bondarenko.tm.constant.TerminalConstant;
import ru.t1.bondarenko.tm.controller.CommandController;
import ru.t1.bondarenko.tm.controller.ProjectController;
import ru.t1.bondarenko.tm.controller.ProjectTaskController;;
import ru.t1.bondarenko.tm.controller.TaskController;
import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.model.Project;
import ru.t1.bondarenko.tm.model.Task;
import ru.t1.bondarenko.tm.repository.CommandRepository;
import ru.t1.bondarenko.tm.repository.ProjectRepository;
import ru.t1.bondarenko.tm.repository.TaskRepository;
import ru.t1.bondarenko.tm.service.CommandService;
import ru.t1.bondarenko.tm.service.ProjectService;
import ru.t1.bondarenko.tm.service.ProjectTaskService;
import ru.t1.bondarenko.tm.service.TaskService;
import ru.t1.bondarenko.tm.util.TerminalUtil;

public class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskController taskController = new TaskController(taskService);

    @Override
    public void run(final String... args) {
        initDemoData();
        parseArguments(args);
        parseCommands();
    }

    private void parseCommands() {
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    private void exit() {
        System.exit(0);
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArgumentError();
        }
        exit();
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case TerminalConstant.VERSION:
                commandController.showVersion();
                break;
            case TerminalConstant.HELP:
                commandController.showHelp();
                break;
            case TerminalConstant.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConstant.PROJECT_SHOW_BY_ID:
                projectController.showProjectByID();
                break;
            case TerminalConstant.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectByID();
                break;
            case TerminalConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConstant.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectByID();
                break;
            case TerminalConstant.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConstant.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConstant.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConstant.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConstant.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConstant.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConstant.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConstant.TASK_SHOW_BY_ID:
                taskController.showTaskByID();
                break;
            case TerminalConstant.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConstant.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectID();
                break;
            case TerminalConstant.TASK_UPDATE_BY_ID:
                taskController.updateTaskByID();
                break;
            case TerminalConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConstant.TASK_REMOVE_BY_ID:
                taskController.removeTaskByID();
                break;
            case TerminalConstant.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConstant.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConstant.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConstant.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConstant.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConstant.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConstant.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConstant.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConstant.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConstant.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void initDemoData() {
        projectService.add(new Project("A Project", Status.COMPLETED));
        projectService.add(new Project("D Project", Status.IN_PROGRESS));
        projectService.add(new Project("C Project", Status.IN_PROGRESS));
        projectService.add(new Project("B Project4", Status.NOT_STARTED));

        taskService.add(new Task("E Task", "Task1"));
        taskService.add(new Task("A Task", "Task2"));
        taskService.add(new Task("F Task", "Task3"));
    }

}
