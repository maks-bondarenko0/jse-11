package ru.t1.bondarenko.tm.service;

import ru.t1.bondarenko.tm.api.repository.ICommandRepository;
import ru.t1.bondarenko.tm.api.service.ICommandService;
import ru.t1.bondarenko.tm.model.Command;

public final class CommandService implements ICommandService {
    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
