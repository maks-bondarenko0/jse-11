package ru.t1.bondarenko.tm.controller;

import ru.t1.bondarenko.tm.api.controller.ITaskController;
import ru.t1.bondarenko.tm.api.service.ITaskService;
import ru.t1.bondarenko.tm.enumerated.Sort;
import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.model.Task;
import ru.t1.bondarenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService.findAll(sort);
        showTaskList(tasks);
        System.out.println("[END]");
    }

    public void showTaskList(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.printf("%s[%s]. %s \n", index, task.getId(), task);
            index++;
        }
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.deleteAll();
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByID() {
        System.out.println("[DELETE TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeByID(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[DELETE TASK BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    @Override
    public void showTaskByID() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findByID(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByProjectID() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("Enter Project id:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectID(projectId);
        showTaskList(tasks);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByID() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByID(id, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatusById(id, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatusByIndex(index, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
