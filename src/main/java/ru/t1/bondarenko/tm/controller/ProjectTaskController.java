package ru.t1.bondarenko.tm.controller;

import ru.t1.bondarenko.tm.api.controller.IProjectTaskController;
import ru.t1.bondarenko.tm.api.service.IProjectTaskService;
import ru.t1.bondarenko.tm.model.Task;
import ru.t1.bondarenko.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter Project ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter Task ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskToProject(projectId, taskId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("Enter Project ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter Task ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else System.out.println("[OK]");
    }

}
