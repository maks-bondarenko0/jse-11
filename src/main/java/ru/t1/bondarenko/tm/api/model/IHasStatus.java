package ru.t1.bondarenko.tm.api.model;

import ru.t1.bondarenko.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
