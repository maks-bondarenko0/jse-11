package ru.t1.bondarenko.tm.api.controller;

public interface ICommandController {

    void showCommandError();

    void showArgumentError();

    void showWelcome();

    void showDeveloperInfo();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

}
